# README #

* Summary of set up

Eclipse with ADT of android
-adt-bundle-windows-x86_64-20140702.zip
  https://dl.google.com/android/adt/adt-bundle-windows-x86_64-20140702.zip

System requeriments:

Windows XP (32-bit), Vista (32- or 64-bit), or Windows 7 (32- or 64-bit)
Mac OS X 10.5.8 or later (x86 only)
Linux (tested on Ubuntu Linux, Lucid Lynx)
GNU C Library (glibc) 2.7 or later is required.
On Ubuntu Linux, version 8.04 or later is required.
64-bit distributions must be capable of running 32-bit applications.